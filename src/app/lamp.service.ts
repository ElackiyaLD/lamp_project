import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http'
import { UserInfo } from './user-info';
import { AdManagementDetails } from './ad-management-details';
import { SearchRequest } from './search-request';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LampService {
  
  baseUrl = environment.baseUrl;
  constructor(private _http:HttpClient) {}

  public loginUserFromRemote(userInfo:UserInfo):Observable<any>
  {
    return this._http.post<any>(this.baseUrl+"/login",userInfo);
  }

  public fetchAdDetailsBySearchCriteria(searchRequest:SearchRequest):Observable<any>
  {
    return this._http.post<any>(this.baseUrl+"/search",searchRequest);
  }

  public fetchJournalInfoByPublisherId(publisher_id:string):Observable<any>
  {
    let params = new HttpParams();
    params = params.append('publisherid', publisher_id);
    return this._http.post<any>(this.baseUrl+"/fetchjournaldetails",params);
  }

  public addRecord(admanagementDtails:AdManagementDetails):Observable<AdManagementDetails>
  {
    console.log("Add Record :"+admanagementDtails);
    return this._http.post<any>(this.baseUrl+"/insert", admanagementDtails);
  }

  public updateRecord(admanagementDtails:AdManagementDetails):Observable<AdManagementDetails>
  {
    console.log("Update Record :"+JSON.stringify(admanagementDtails));
    return this._http.put<any>(this.baseUrl+"/update", admanagementDtails);
  }

  public updateActiveStatus(admanagementDtails:AdManagementDetails):Observable<AdManagementDetails>
  {
    console.log("Update Active Status Record :"+JSON.stringify(admanagementDtails));
    return this._http.put<any>(this.baseUrl+"/updateActiveStatus",admanagementDtails);
  }

  public fetchSearchYears():Observable<any>
  {
    return this._http.post<any>(this.baseUrl+"/fetchsearchyears","");
  }

  public fetchYears():Observable<any>
  {
    return this._http.post<any>(this.baseUrl+"/fetchyears","");
  }
  public getNoOfActiveRecords(admanagementDtails:AdManagementDetails):Observable<any>
  {
    return this._http.put<any>(this.baseUrl+"/getNoOfActiveRecords",admanagementDtails);
  }

  public fetchConfig(publisher_id:string,field:string):Observable<any>
  {
    let params = new HttpParams();
    params = params.append('publisherid', publisher_id);
    params = params.append('field', field);
    return this._http.post<any>(this.baseUrl+"/fetchconfig",params);
  }

  public save(fileData: any):void
  {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    let options = { headers: headers };
    this._http.post<any>(this.baseUrl+"/upload",fileData,options)
  }


  public download(ldlFileName:string,journalId:string)
  {

//     const headers = new HttpHeaders({'content-type' : 'application/json',
//     'Access-Control-Allow-Origin' : '*'
//   });
 const REQUEST_PARAM = new HttpParams().set('fileName', ldlFileName).set('journalId',journalId);
    return this._http.get(this.baseUrl+"/download",{params:REQUEST_PARAM, responseType : 'arraybuffer'});
  }
}
