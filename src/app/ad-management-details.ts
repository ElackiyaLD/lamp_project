export class AdManagementDetails {
    advertId:string;
	advertTitle:string;
	journalid:string;
	journalName:string;
	issueId:string;
	year:string;
	color_mode:string;
	advertSize:string;
	category:string;
	submittedDate:string;
	clientFileName:string;
    ldlFileName:string;
	activeStatus : string;
    
    constructor()
    {

    }
}
