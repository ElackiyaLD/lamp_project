import { Component, OnInit } from '@angular/core';
import { LampService } from '../lamp.service';
import { UserInfo } from '../user-info';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userInfo = new UserInfo();
  loginMessage = '';

  constructor(private _service: LampService, private _router :Router ) { }

  ngOnInit(): void {

    
  }

  loginUser()
  {
    this._service.loginUserFromRemote(this.userInfo).subscribe(
      data => {
        console.log("response received");
        console.log(this.userInfo.userName);
        console.log(data.userName);
        console.log(data.roleId);
        localStorage.setItem("loggedUser", data.userName);
        localStorage.setItem("roleId", data.roleId);
        this._router.navigate(['/lampmainpage'])
    },
      error => {
        console.log("Exception occured");
        this.userInfo = new UserInfo();
        this.loginMessage="Login failed : Invalid email address or password. "
      }
    )
  }

}
