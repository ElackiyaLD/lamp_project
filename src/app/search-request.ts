import { JournalDetails } from "./journal-details";

export class SearchRequest {
    journalId:JournalDetails[];
    advertId:string;
    issueId:string;
    year:number;
    fileName:string;

    constructor(){
        
    }

}
