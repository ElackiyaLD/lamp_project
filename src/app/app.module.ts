import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LampmainpageComponent } from './lampmainpage/lampmainpage.component';
import { GridModule, PagerModule, PageService,FilterService,SortService,EditService,ToolbarService,ExcelExportService,DataSourceChangedEventArgs, dataSourceChanged,ResizeService } from '@syncfusion/ej2-angular-grids';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AccordionModule, ToolbarModule, ContextMenuModule, TabModule, TreeViewModule, SidebarModule, MenuModule } from '@syncfusion/ej2-angular-navigations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DatePipe } from '@angular/common';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LampmainpageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    GridModule, PagerModule,
    NgMultiSelectDropDownModule.forRoot(),
    AccordionModule, ToolbarModule, ContextMenuModule, TabModule, TreeViewModule, SidebarModule, MenuModule,FontAwesomeModule,DialogModule,PdfViewerModule,ToastModule],
  providers: [PageService,FilterService,SortService,EditService,ToolbarService,ExcelExportService,DatePipe,ResizeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
