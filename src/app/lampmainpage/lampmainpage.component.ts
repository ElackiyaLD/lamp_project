import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { LampService } from '../lamp.service';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, zip } from 'rxjs';
import { AdManagementDetails } from '../ad-management-details';
import {DataStateChangeEventArgs,GridComponent,DataSourceChangedEventArgs, AddEventArgs, EditEventArgs, SaveEventArgs, PredicateModel, IEditCell, IFilter, filterCmenuSelect, ExcelExportService,ExcelExportProperties, RowDataBoundEventArgs} from '@syncfusion/ej2-angular-grids';

import {GridModule, PagerModule, PageService, PageSettingsModel, FilterService, SortService,EditSettingsModel,ToolbarItems,FilterSettingsModel,Column,QueryCellInfoEventArgs,rowDataBound} from '@syncfusion/ej2-angular-grids';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { JournalDetails } from '../journal-details';
import { SearchRequest } from '../search-request';
import { ClickEventArgs, EventArgs } from '@syncfusion/ej2-angular-navigations';
import { Uploader,UploadingEventArgs,SelectedEventArgs } from '@syncfusion/ej2-inputs'; 
import { DropDownList } from '@syncfusion/ej2-dropdowns';
import { Tooltip } from '@syncfusion/ej2-popups';
import { saveAs } from 'file-saver';
import { DialogComponent,DialogUtility } from '@syncfusion/ej2-angular-popups';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { environment } from '../../environments/environment';
import { L10n, setCulture } from '@syncfusion/ej2-base';

setCulture('en-US');

L10n.load({
    'en-US': {
        'grid': {
          'Update': 'Reuse'
        },
      }
    })

const MIME_TYPES = {
pdf:'application/pdf',
jpeg: 'images/jpeg',
jpg: 'images/jpeg'
};



@Component({
  selector: 'app-lampmainpage',
  templateUrl: './lampmainpage.component.html',
  styleUrls: ['./lampmainpage.component.css']
})


export class LampmainpageComponent implements OnInit {

  baseUrl = environment.baseUrl;
  //local variables
  publisher_id="P1034"; 
  currentYear:any;
  responseMessage:string;
  //public datepipe: DatePipe;
  searchRequest = new SearchRequest();
  public elem: HTMLElement;
  public uploadObj:Uploader;

  public jounralElement:HTMLElement;
  public journalDropDown:DropDownList;

  public addEditYearElement:HTMLElement;
  public addEditYearDropDown:DropDownList;

  public advertSizeElement:HTMLElement;
  public advertSizeDropDown:DropDownList;

  public colorModeElement:HTMLElement;
  public colorModeDropDown:DropDownList;

  public categoryElement:HTMLElement;
  public categoryDropDown:DropDownList;

  public jounralNameElement:HTMLElement;
  public journalNameText:Text;
  
  @ViewChild('toastelement') public element: ToastComponent;
  public position = { X: 'Center', Y:'Top'};


  @ViewChild('modalDialog') public modalDialog: DialogComponent;
  public target: string;
  public isModal: boolean;

  //Save Propmt model
  @ViewChild('confirmationDialog') public confirmationDialog: DialogComponent;

  public targetCD:string;
  public imgSrc: any;
  public dlg_title:string; 
  public confirmationHeader:string="Confirmation"; 
  public confirmationContentMessage:string="File is not uploaded. Do you want to upload the modified Ad file before proceed to save?"; 
 
  public animationSettings: Object = { effect: 'Zoom', duration: 600, delay: 0 };

  //Validation
  advertIdRules:object;
	advertTitleRules:object;
	journalRules:object;
	issueIdRules:object;
	yearRules:object;
  fileRules:object;
	color_modeRules:object;
	advertSizeRules:object;
	categoryRules:object;
  path: Object;

  //Settings
  filterOptions:IFilter={operator:"contains" };
  filterSettings:FilterSettingsModel={mode:"Immediate", immediateModeDelay:250};
  numericParams:IEditCell;
  uploadParams:IEditCell;
  yearParam:IEditCell;
  journalddParams:IEditCell;
  advertSizeParams:IEditCell;
  colorModeParams:IEditCell;
  categoryParams:IEditCell;
  pageSettings:PageSettingsModel = {pageSize:50};
  editOption:EditSettingsModel; 
  toolbarOption:ToolbarItems[]=['Add','Edit',{ text: 'Reuse', tooltipText: 'Reuse', prefixIcon: 'e-edit', id: 'Reuse' },'ExcelExport'];
  
  @ViewChild('grid') public grid: GridComponent;

  dropdownSettings:IDropdownSettings = {
    singleSelection: false,
    idField: 'journalId',
    textField: 'journalId',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 50,
    allowSearchFilter: true
  };

  adManagementDetails:Observable<AdManagementDetails>;
  adManagementDetailList :Observable<AdManagementDetails[]>;
  journalDetailsList : Observable<JournalDetails[]>;
  selectedItems:Observable<JournalDetails[]>;
  yearList:string[];
  loggedUser:string;
  roleId:string;
  advertSizeList:string[];
  addEditYearList:string[];
  colorModeList:string[];
  categoryList:string[];
  //
  //data : Observable<AdManagementDetails>;
  rowNumber:number;
  selectedRowDetail: AdManagementDetails;
  downloadRowDetail:AdManagementDetails;
  journalParams: IEditCell;
  journalNameParams: IEditCell;
  selectedFileName:string;
  modeParams: IEditCell;
  yearParams: IEditCell;
  pdfSrc:ArrayBuffer;
  pdfVisible:string;
  activeStatus : string
  //
  saveEventArgument:SaveEventArgs;
  constructor(private _service: LampService, private _router :Router,private sanitizer: DomSanitizer,private datepipe: DatePipe) { 
  }

  ngOnInit(): void {

  this.target="#mainDiv"; 
  this.targetCD="#GridEditForm"; 
  this.isModal=true;
  this.loggedUser = localStorage.getItem("loggedUser");  
  this.roleId = localStorage.getItem("roleId");  
  this.currentYear=(new Date()).getFullYear();
  //ela
//is.rowDataBound(this.targetCD);

  //Dropdown Population
  this._service.fetchJournalInfoByPublisherId(this.publisher_id).subscribe(res=>this.journalDetailsList=res);
  this._service.fetchSearchYears().subscribe(res=>this.yearList=res);
  this._service.fetchYears().subscribe(res=>this.addEditYearList=res);
  this._service.fetchConfig(this.publisher_id,"advert_size").subscribe(res=>this.advertSizeList=res);
  this._service.fetchConfig(this.publisher_id,"color_mode").subscribe(res=>this.colorModeList=res);
  this._service.fetchConfig(this.publisher_id,"category").subscribe(res=>this.categoryList=res);

  //Validation
  this.advertIdRules = { required: [this.customValidation,"This field is required"],maxLength:50,regex:['^[A-z0-9 -]+$','Alphanumeric,space,_ and - are only allowed']};
  this.advertTitleRules = { required: [this.customValidation,"This field is required"], maxLength:200,regex:['^[A-z0-9 -]+$','Alphanumeric,space,_ and - are only allowed'] };
  this.journalRules = { required: true };
  this.issueIdRules = { required: [this.customValidation,"This field is required"] ,maxLength:10,regex:['^[A-z0-9 -]+$','Alphanumeric,space,_ and - are only allowed'] };
  this.yearRules = { required: true,  };
  this.color_modeRules = { required: true};
  this.advertSizeRules = { required: true };
  this.categoryRules = { required: true};
  this.numericParams = { params: { decimals: 0, value: 4  } };
  
   

  //Customer Role - Full Access - Others Add option will not be available
  if("RM1018" == this.roleId)
  {
    this.editOption ={allowAdding:true, allowEditing:true, mode:'Dialog', newRowPosition:'Bottom'}; 
    
    this.toolbarOption=['Add','Edit','ExcelExport'];
  }
  else{
    this.editOption ={allowAdding:false, allowEditing:true,mode:'Dialog', newRowPosition:'Bottom'}; 
    this.toolbarOption=['Edit','ExcelExport'];
  }
  
  
 
 this.path = {
    saveUrl: this.baseUrl+"/upload"
    
  }

  //Upload Component - Edit param
  this.uploadParams = { 
    create: () => { 
        this.elem = document.createElement('input'); 
       // this.elem.setAttribute('data-required-message', 'Choose file to upload');
       // this.elem.setAttribute('required','');
        return this.elem; 
    }, 
    read: () => { 
        //Here return the value to be updated in Grid 
        return this.selectedFileName;
        
    }, 
    destroy: () => { 
    }, 
   write: (args: { rowData: Object, column: Column }) => { 
        this.uploadObj = new Uploader({ 
            
            autoUpload: true,
            multiple:false, 
            asyncSettings:this.path,
            allowedExtensions:'.jpg,.png,.pdf,.eps',
            selected: (sea : SelectedEventArgs) => {
            console.log(this.uploadObj.getFilesData.length);
             // console.log(this.uploadObj.getFilesData.arguments);
             this.selectedFileName = sea.filesData[0].name;
          },
          
          uploading:(uea : UploadingEventArgs)=>{
            const formData: FormData = new FormData();
            formData.append("file", uea.fileData.rawFile,uea.fileData.name);
            console.log(JSON.stringify(uea.fileData));
            this._service.save(uea.fileData.rawFile);
          }
        }); 
        // console.log("U : "+this.uploadObj.filesData);
        //this.selectedFileName=this.uploadObj.files[0].name;
       
        this.uploadObj.appendTo(this.elem); 
    }
}; 

//Journal Details Dropdown list - Edit Param
this.journalddParams = { 

  create: () => { 
      this.jounralElement = document.createElement('input'); 
      return this.jounralElement; 
  }, 
  read: () => { 
      //Here return the value to be updated in Grid 
      return this.journalDropDown.text;
  }, 
  destroy: () => { 
  }, 
 write: (args: { rowData: Object, column: Column }) => { 
      this.journalDropDown = new DropDownList({ 
          dataSource: JSON.parse(JSON.stringify(this.journalDetailsList)),
          value:args.rowData[args.column.field],
          fields: { value: 'journalTitle', text: 'journalTitle'},
          placeholder: 'Select a Journal',
         allowFiltering:true
      }); 
      this.journalDropDown.appendTo(this.jounralElement); 
  } 
};

//Year Dropdown list - Edit Param
this.yearParam = { 
  create: () => { 
      this.addEditYearElement = document.createElement('input'); 
      return this.addEditYearElement; 
  }, 
  read: () => { 
      //Here return the value to be updated in Grid 
      return this.addEditYearDropDown.text;
  }, 
  destroy: () => { 
  }, 
 write: (args: { rowData: Object, column: Column }) => { 
  //console.log(Number(args.rowData[args.column.field]));
      //console.log(this.addEditYearList.indexOf(args.rowData[args.column.field]));
      this.addEditYearDropDown = new DropDownList({ 
          dataSource: this.addEditYearList,
          value:args.rowData[args.column.field],
          fields: { value: 'year', text: 'year'},
          placeholder: 'Select a Year',
         allowFiltering:true
      }); 
      this.addEditYearDropDown.appendTo(this.addEditYearElement); 
  } 
};

//Advert Size Dropdown list - Edit Param
this.advertSizeParams = { 
  create: () => { 
      this.advertSizeElement = document.createElement('input'); 
      return this.advertSizeElement; 
  }, 
  read: () => { 
      //Here return the value to be updated in Grid 
      return this.advertSizeDropDown.text;
  }, 
  destroy: () => { 
  }, 
 write: (args: { rowData: Object, column: Column }) => { 
      this.advertSizeDropDown = new DropDownList({ 
          dataSource: this.advertSizeList,
          value:args.rowData[args.column.field],
          fields: { value: 'advertSize', text: 'advertSize'},
          placeholder: 'Select a Advert Size',
          allowFiltering:true
      }); 
      this.advertSizeDropDown.appendTo(this.advertSizeElement); 
  } 
};   
// Active status
// this.activestatusParam = { 
//   create: () => { 
//       this.activeStatusElement = document.createElement('input'); 
//       return this.activeStatusElement; 
//   }, 
//   read: () => { 
//       //Here return the value to be updated in Grid 
//       return this.activeStatusDropDown.text;
//   }, 
//   destroy: () => { 
//   }, 
//  write: (args: { rowData: Object, column: Column }) => { 
//       this.activeStatusDropDown = new DropDownList({ 
//           dataSource: this.activeStatusList,
//           value:args.rowData[args.column.field],
//           fields: { value: 'status', text: 'activeStatus'},
//           placeholder: 'Active/Inactive',
//           allowFiltering:true
//       }); 
//       this.activeStatusDropDown.appendTo(this.activeStatusElement); 
//   } 
// };

//Color Mode Dropdown list - Edit Param
this.colorModeParams = { 
  create: () => { 
      this.colorModeElement = document.createElement('input'); 
      return this.colorModeElement; 
  }, 
  read: () => { 
      //Here return the value to be updated in Grid 
      return this.colorModeDropDown.text;
  }, 
  destroy: () => { 
  }, 
 write: (args: { rowData: Object, column: Column }) => { 
      this.colorModeDropDown = new DropDownList({ 
          dataSource: this.colorModeList,
          value:args.rowData[args.column.field],
          fields: { value: 'colorMode', text: 'colorMode'},
          placeholder: 'Select a Color Mode',
          allowFiltering:true
      }); 
      this.colorModeDropDown.appendTo(this.colorModeElement); 
  } 
};  

//Category Dropdown list - Edit Param
this.categoryParams = { 
  create: () => { 
      this.categoryElement = document.createElement('input'); 
      return this.categoryElement; 
  }, 
  read: () => { 
      //Here return the value to be updated in Grid 
      return this.categoryDropDown.text;
  }, 
  destroy: () => { 
  }, 
 write: (args: { rowData: Object, column: Column }) => { 
      this.categoryDropDown = new DropDownList({ 
          dataSource: this.categoryList,
          value:args.rowData[args.column.field],
          fields: { value: 'category', text: 'category'},
          placeholder: 'Select a Category',
          allowFiltering:true
      }); 
      this.categoryDropDown.appendTo(this.categoryElement); 
  } 
};  
}


onItemSelect(item: string) {
  }
onSelectAll(items: string) {
  }

  //Download File 
  clicked(args: any, data:any)
  {
    console.log(args);

     this.downloadRowDetail = JSON.parse(JSON.stringify(data));
     let fileName : string = this.downloadRowDetail.ldlFileName; 
     let journalid : string = this.downloadRowDetail.journalid; 
      
     this._service.download(fileName,journalid).subscribe(data => 
      {
        const EXT = fileName.substr(fileName.lastIndexOf('.')+1);
        saveAs(new Blob([data] , {type : MIME_TYPES[EXT]}), fileName)
      });
  }

  //View File 
  viewClicked(args: any, data:any)
    {
      this.modalDialog.show();
      this.downloadRowDetail = JSON.parse(JSON.stringify(data));
      console.log(this.downloadRowDetail);
     let fileName : string = this.downloadRowDetail.ldlFileName; 
     this.dlg_title = "Preview - "+fileName; 
     this.pdfSrc=new ArrayBuffer(0); 
     this.imgSrc="";
     let journalid : string = this.downloadRowDetail.journalid; 
     this._service.download(fileName,journalid).subscribe(data => 
      {
        const EXT = fileName.substr(fileName.lastIndexOf('.')+1);
        if(EXT == 'pdf')
        {
          this.pdfSrc=data;
        }
        else{
          const blob = new Blob([data]);
          const unsafeImg = URL.createObjectURL(blob);
         this.imgSrc = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
     }  
    });
  }
  private okClick(): void {
    //alert('you clicked OK button');
    //var activeStatus="Inactive"
    //this.searchAction();

    //this.selectedRowDetail = JSON.parse(this.data);
    this._service.updateActiveStatus(this.selectedRowDetail).subscribe(
            res=>{
              //this.selectedRowDetail=res;
              
              this.element.width="auto";
              this.element.cssClass="toastSucces";
              this.element.content="<span><i class=\"fa fa-check-circle\" aria-hidden=\"true\" style=\"color:#3c763d;font-size:25px;\"></i><label style=\"color:#3c763d;font-size: 16px;margin-left:10px;\">Advert deactivated Successfully</label></span>";
              this.element.show();
            },
            error => {
              console.log("Exception occured"+error.message);
              this.element.width="auto";
              this.element.cssClass="toastFailed";
              this.element.content="<span><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\" style=\"color:#a94442;font-size:25px;\"></i><label style=\"color:#a94442;font-size: 16px;margin-left:10px;\">Advert is already Inactive</label></span>";
              
              this.element.show();
            });
     //.this.ngOnInit();
     //this.searchAction();

 }
 private cancelClick(): void {
  //alert('you clicked Cancel button');
}

// rowDataBound(args) { 
//  //alert(args.data["InActive"]);
//    this.selectedRowDetail = JSON.parse(JSON.stringify(args.data));
     
//         if (this.selectedRowDetail.activeStatus== "InActive") {//alert(args.data["InActive"]);
//             args.row.css("background-color", 'grey');//change the row background color based on status
//         }
 
 
//     }
  //View File 
  MakeAdInactive(args: any, data:any)
    {
      //alert("Ad deactivated");
      this._service.getNoOfActiveRecords(data).subscribe(
            res=>{
              //this.selectedRowDetail=res;
              DialogUtility.confirm({
        title: 'Confirm',
        content: "<h5>"+res+" records will be Inactive. Proceed?</h5>",
        //okButton: {  text: 'OK', click: this.searchAction },
        okButton: {  text: 'OK', click: this.okClick.bind(this) },
        //cancelButton: {  text: 'Cancel', click: this.cancelClick.bind(this) },
        showCloseIcon: true,
        closeOnEscape: true,
        animationSettings: { effect: 'Zoom' }
      });
              
            },
            error => {
              
            });
              //this.selectedRowDetail=res;
      this.searchRequest = JSON.parse(JSON.stringify(data));
      console.log(this.adManagementDetails);
      this.selectedRowDetail = data;

      
       
    
  }

public actionBegin(args:SaveEventArgs)
{
  
  console.log(args.requestType);
  console.log(args.action);
  
  if ((args.requestType === 'beginEdit' || args.requestType === 'add') || args.requestType === 'Reuse') 
  {console.log("hello "+this.grid.getSelectedRecords());console.log("hello2 "+args);
    for (const cols of this.grid.columns) {
      if ((cols as Column).field === 'downloadView') {
        (cols as Column).visible = false;
    }
    }

  }

  if(args.action == 'add')
  {
    var argsDetails: AdManagementDetails;
    argsDetails =JSON.parse(JSON.stringify(args.data));
    
    //console.log("fn :"+this.selectedRowDetail.clientFileName );
    //console.log("fn :"+this.uploadObj.element.files );
    
     if(argsDetails.clientFileName == undefined || argsDetails.clientFileName == null)
     {
       alert("Please upload a file to proceed");
       args.cancel=true;
     }
     else{
      args.cancel=false;
     }

  // if(this.selectedRowDetail.clientFileName == null)
  // {
  //   args.cancel=true;
  //   this.saveEventArgument =args;
  //   this.confirmationDialog.show();
  //   if(this.confirmationDialog.)
  // //  if(window.confirm(this.confirmationContentMessage))
  // //  {
  // //   args.cancel=true;
  // //  }
  // //  else
  // //  {
  // //   args.cancel=false;
  // //  }
  // }

}  
}

  //ADD EDIT and Update action
public actionComplete(args:any) {

  if(args.requestType === 'add')
  {
    const dialog = args.dialog;
    dialog.header="New Advert Details";
    this.selectedFileName=undefined;
  }  
  else if(args.requestType === 'beginEdit')
  {
    console.log(args);
  const dialog = args.dialog;
  dialog.header="Edit Advert Details";
  }
  else if(args.action == 'edit')
  {
     this.selectedRowDetail = JSON.parse(JSON.stringify(args.data));

     this._service.updateRecord(this.selectedRowDetail).subscribe(
            res=>{
              //this.selectedRowDetail=res;
              
              this.element.width="auto";
              this.element.cssClass="toastSucces";
              this.element.content="<span><i class=\"fa fa-check-circle\" aria-hidden=\"true\" style=\"color:#3c763d;font-size:25px;\"></i><label style=\"color:#3c763d;font-size: 16px;margin-left:10px;\">Record has been updated successfully.</label></span>";
              this.element.show();
            },
            error => {
              console.log("Exception occured");
              this.element.width="auto";
              this.element.cssClass="toastFailed";
              this.element.content="<span><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\" style=\"color:#a94442;font-size:25px;\"></i><label style=\"color:#a94442;font-size: 16px;margin-left:10px;\">Record has not been updated.</label></span>";
              
              this.element.show();
            });
     this.ngOnInit();
     this.searchAction();
    // this.grid.refresh();
   
  }
  else if(args.action == 'add')
  {
     this.selectedRowDetail = JSON.parse(JSON.stringify(args.data));
      this._service.addRecord(this.selectedRowDetail).subscribe(
        res=>{
          //this.selectedRowDetail=res;
          this.element.width="auto";
              this.element.cssClass="toastSucces";
              this.element.content="<span><i class=\"fa fa-check-circle\" aria-hidden=\"true\" style=\"color:#3c763d;font-size:25px;\"></i><label style=\"color:#3c763d;font-size: 16px;margin-left:10px;\">Record has been inserted successfully.</label></span>";
              this.element.show();
        },
        error => {
          console.log("Exception occured");
          this.element.width="auto";
          this.element.cssClass="toastFailed";
          this.element.content="<span><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\" style=\"color:#a94442;font-size:25px;\"></i><label style=\"color:#a94442;font-size: 16px;margin-left:10px;\">Record has not been inserted.</label></span>";
          
          this.element.show();
        });
        this.ngOnInit();
     this.searchAction();

  }

  for (const cols of this.grid.columns) {
    if ((cols as Column).field === 'downloadView') {
      (cols as Column).visible = true;
  }
  }
}

  //Search Button Action
  searchAction()
  {
    console.log(this.searchRequest);
    this._service.fetchAdDetailsBySearchCriteria(this.searchRequest).subscribe(
      
      res=>this.adManagementDetailList=res);
  }

  //Excel Export
  toolbarClick(args: any): void {
   if (args.item.id === 'Grid_excelexport') { // 'Grid_excelexport' -> Grid component id + _ + toolbar item name
    let latest_date =this.datepipe.transform(new Date(), 'yyyyMMdd_HHmmss');
    console.log(latest_date);
    const excelExportProperties: ExcelExportProperties = {
      fileName:'advert_'+latest_date+".xlsx"
  };
        this.grid.excelExport(excelExportProperties);
    }
    if (args.item.id === 'Reuse') {  
      this.editOption ={allowAdding:true, allowEditing:true, mode:'Dialog', newRowPosition:'Bottom'}; 
      alert('Custom Toolbar Click...'+this.grid.getSelectedRecords().copyWithin(10,1,12));console.log("helelo "+this.grid.getSelectedRecords());
      this.grid.dataSource=this.grid.getSelectedRecords();
      args.requestType='Reuse';
      args.action='edit';
      args.data=this.grid.getSelectedRecords();
      const dialog = args.dialog;
  //args.dialog.header="Edit Advert Details";
      this.actionBegin(args);
      this.actionComplete(args);
      //this.selectedFileName
      //this.grid.addRecord(this.grid.getSelectedRecords(),10)
      //this.saveEventArgument.requestType='beginEdit';
      // SaveEventArgument z 
      // z.action='edit';
      // this.saveEventArgument.data=this.grid.getSelectedRecords();
      // this.actionBegin(this.saveEventArgument)
      //this.grid.updateRow(10,this.grid.getSelectedRecords());
      for (const cols of this.grid.columns) {
        if ((cols as Column).field === 'downloadView') {
          (cols as Column).visible = false;
      }
      }
      var argsDetails: AdManagementDetails;
      argsDetails =JSON.parse(JSON.stringify(args.data));

      this.selectedRowDetail = JSON.parse(JSON.stringify(args.data));
    //   this._service.updateRecord(this.grid.getSelectedRecords()).subscribe(
    //     res=>{
    //       //this.selectedRowDetail=res;
    //       this.element.width="auto";
    //           this.element.cssClass="toastSucces";
    //           this.element.content="<span><i class=\"fa fa-check-circle\" aria-hidden=\"true\" style=\"color:#3c763d;font-size:25px;\"></i><label style=\"color:#3c763d;font-size: 16px;margin-left:10px;\">Record has been inserted successfully.</label></span>";
    //           this.element.show();
    //     },
    //     error => {
    //       console.log("Exception occured");
    //       this.element.width="auto";
    //       this.element.cssClass="toastFailed";
    //       this.element.content="<span><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\" style=\"color:#a94442;font-size:25px;\"></i><label style=\"color:#a94442;font-size: 16px;margin-left:10px;\">Record has not been inserted.</label></span>";
          
    //       this.element.show();
    //     });
    //     this.ngOnInit();
    //  this.searchAction();
  
  }
  }

  //Tooltip
  tooltip(args: QueryCellInfoEventArgs) {
    if(args.data != undefined && args.column.field !=undefined  && args.data[args.column.field]!=undefined)
    {
    // you can also add tooltip based on condition here 
    let tooltip: Tooltip = new Tooltip({
      content: args.data[args.column.field].toString()
    }, args.cell as HTMLElement);
  }
  }
  rowDataBound(args: RowDataBoundEventArgs) {
    
    this.selectedRowDetail = JSON.parse(JSON.stringify(args.data));
    
    if(this.selectedRowDetail.activeStatus=='Inactive') {
        rowDataBound.fontcolor('grey');
    } 
}

   onCreate(args:any) {
    this.element.show()
  }

  // To avoid to aloow empty string
  customValidation(args:any)
  {
    if(args.value.trim().length > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
}

