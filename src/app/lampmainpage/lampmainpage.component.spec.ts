import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LampmainpageComponent } from './lampmainpage.component';

describe('LampmainpageComponent', () => {
  let component: LampmainpageComponent;
  let fixture: ComponentFixture<LampmainpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LampmainpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LampmainpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
